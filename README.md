Understanding Uncertainty in the American Community Survey
===========================================

Spielman, S.E., Folch D., Nagle, N. *__in press__* *Applied Geography*
------------------------------

This site contains both the data and the code required to reproduce the figures in our *Applied Geography* article on the ACS.  The data are all from 2007-2011 release of the ACS and include all census tracts in the US.  

The code is written in R, with dependencies limited to the libaraies explictly imported at the head of the script.  

After the creation of the figures thre is a bit of code to show how the gini coefficient relates to variance in the population.  This is not explictly used in the article but supports our argument about income diversity and sampling error.